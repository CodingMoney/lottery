package xin.xihc.lottery;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * TODO
 *
 * @author Leo.Xi
 * @date 2022/2/26
 * @since 1.0
 **/
public class TestJsoup {

    @Test
    public void testJsoup() throws IOException {
        //        Document document = Jsoup.connect("https://www.cjcp.com.cn/kaijiang/")
        //                                 .get();
        //
        //        Elements qgkjTables = document.getElementsByClass("qgkj_table");

        Document document = Jsoup.connect("https://www.cjcp.com.cn/zoushitu/cjwdlt/qqdingwei-01.html")
                                 .data("stq", "2021001")
                                 .data("enq", "2022019")
                                 .data("st", "1")
                                 .post();
        Elements children = document.selectFirst("#tabledata #pagedata")
                                    .children();
        for (Element child : children) {
            //            String index = child.child(0).text();
            String qishu = child.child(1)
                                .text();
            String nums = child.child(2)
                               .text();
            System.err.println(String.format("%s-%s", qishu, nums));
        }

        document = Jsoup.connect("https://www.cjcp.com.cn/zoushitu/cjwssq/hqzonghe-2-100.html")
                        .get();
        children = document.selectFirst("#tabledata #pagedata")
                           .children();
        for (Element child : children) {
            //            String index = child.child(0).text();
            String qishu = child.child(1)
                                .text();
            String nums = child.child(2)
                               .text();
            System.out.println(String.format("%s-%s", qishu, nums));
        }
    }


}