package xin.xihc.lottery.param;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import xin.xihc.lottery.LotteryType;

import javax.validation.constraints.NotNull;

/**
 * TODO
 *
 * @author Leo.Xi
 * @date 2022/2/27
 * @since 1.0
 **/
@Data
public class DetailParam {
    @NotNull
    private LotteryType type;// 彩票类型
    @NotNull
    @Range(min = 1, max = 7)
    private Integer index;// 号码位 1- 7
    @Range(min = 1, max = 200)
    private Integer qs; // 期数001-200
    @Range(min = 2010)
    private Integer year;// 年份2010-20xx

}