package xin.xihc.lottery.param;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import xin.xihc.lottery.LotteryType;

import javax.validation.constraints.NotNull;

/**
 * 基本参数
 *
 * @author Leo.Xi
 * @date 2022/2/28
 * @since 1.0
 **/
@Data
public class BasicParam {
    @NotNull
    private LotteryType type;// 彩票类型
    @Range(min = 1, max = 200)
    private Integer qs; // 期数001-200
    @Range(min = 2010)
    private Integer year;// 年份2010-20xx

}