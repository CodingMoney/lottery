package xin.xihc.lottery.param;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 机选号码
 *
 * @author Leo.Xi
 * @date 2022/2/27
 * @since 1.0
 **/
@Getter
@Setter
public class AutogenParam extends BasicParam {
    @NotNull
    private Integer limit = 10;//
    @NotNull
    private BigDecimal minScore = new BigDecimal(60);

}