package xin.xihc.lottery.param;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * 手动生成号码参数
 *
 * @author Leo.Xi
 * @date 2022/3/20
 * @since 1.0
 **/
@Getter
@Setter
public class ManualGenNumsParam extends BasicParam {

    @Size(min = 7, max = 7, message = "请选择完整的号码")
    private List<List<String>> nums;

}