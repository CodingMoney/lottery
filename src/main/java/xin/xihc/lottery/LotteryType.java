package xin.xihc.lottery;

import lombok.Getter;

/**
 * 彩票类型
 *
 * @author Leo.Xi
 * @date 2022/2/26
 * @since 1.0
 **/
@Getter
public enum LotteryType {

    SSQ("双色球"),
    DLT("大乐透"),
    ;

    private String title;

    LotteryType(String title) {
        this.title = title;
    }
}
