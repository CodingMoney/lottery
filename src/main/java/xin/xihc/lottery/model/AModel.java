package xin.xihc.lottery.model;

import lombok.Data;
import xin.xihc.jba.annotation.Column;

import java.util.Date;

/**
 * 标准模型
 *
 * @author Leo.Xi
 * @date 2022/2/26
 * @since 1.0
 **/
@Data
public class AModel {

    @Column(primary = true, policy = Column.Policy.AUTO, remark = "自增主键")
    protected Integer id;
    @Column(notNull = true, remark = "创建时间戳", precision = 3, defaultValue = "CURRENT_TIMESTAMP(3)", order = 999)
    protected Date createTime;
    @Column(remark = "用于控制高并发的版本号", defaultValue = "0", notNull = true, order = 999)
    protected Integer version;
}