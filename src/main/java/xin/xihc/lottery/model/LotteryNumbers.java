package xin.xihc.lottery.model;

import lombok.Getter;
import lombok.Setter;
import xin.xihc.jba.annotation.Column;
import xin.xihc.jba.annotation.Table;
import xin.xihc.lottery.LotteryType;

/**
 * 彩票中奖号码
 *
 * @author Leo.Xi
 * @date 2022/2/26
 * @since 1.0
 **/
@Table(value = "lottery_numbers", remark = "彩票中奖号码")
@Getter
@Setter
public class LotteryNumbers extends AModel {

    @Column(remark = "期数", notNull = true, length = 20)
    private String qs;
    @Column(remark = "彩票类型", notNull = true, length = 12)
    private LotteryType type;
    @Column(remark = "数字1", notNull = true)
    private Integer n1;
    @Column(remark = "数字2", notNull = true)
    private Integer n2;
    @Column(remark = "数字3", notNull = true)
    private Integer n3;
    @Column(remark = "数字4", notNull = true)
    private Integer n4;
    @Column(remark = "数字5", notNull = true)
    private Integer n5;
    @Column(remark = "数字6", notNull = true)
    private Integer n6;
    @Column(remark = "数字7", notNull = true)
    private Integer n7;
    @Column(remark = "原始数字", length = 256)
    private String nums;

}