package xin.xihc.lottery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import xin.xihc.jba.annotation.EnableJba;

@SpringBootApplication
@EnableJba
@EnableTransactionManagement
public class LotteryApplication {

    public static void main(String[] args) {
        SpringApplication.run(LotteryApplication.class, args);
    }

}
