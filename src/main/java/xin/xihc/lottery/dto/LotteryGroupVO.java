package xin.xihc.lottery.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * TODO
 *
 * @author Leo.Xi
 * @date 2022/2/27
 * @since 1.0
 **/
@Getter
@Setter
public class LotteryGroupVO extends LotteryGroupDTO {
    private Integer total;
    private BigDecimal rate = BigDecimal.ZERO;
}