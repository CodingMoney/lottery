package xin.xihc.lottery.dto;

import lombok.Data;

/**
 * TODO
 *
 * @author Leo.Xi
 * @date 2022/2/27
 * @since 1.0
 **/
@Data
public class LotteryGroupDTO {
    private Integer num;
    private Integer count = 0;
}