package xin.xihc.lottery.dao;

import org.springframework.stereotype.Repository;
import xin.xihc.lottery.model.LotteryNumbers;

/**
 * DAO
 *
 * @author Leo.Xi
 * @date 2022/2/26
 * @since 1.0
 **/
@Repository
public class LotteryNumbersDao extends ADao<LotteryNumbers> {

}