job "lottery" {
    datacenters = ["dc1"]
    type = "service"

    group "lottery-group" {

        task "lottery-server" {
            driver = "raw_exec"

            config {
				command = "java"
				args        =  [
					"-Xmx1024m",
					"-Xms256m",
					"-jar",
					"local/lottery-0.0.2-SNAPSHOT.jar",
					"--server.port=${NOMAD_PORT_http}"
				]
			}

			artifact {
				source = "http://nomad.xihc.xin:9999/lottery-0.0.2-SNAPSHOT.jar"
			}

		}
		network {
			port "http" { static = 8088 }
		}

    }
}